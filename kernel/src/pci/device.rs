use core::mem::transmute;
use x86_util::{inl, outl};

/// Represents a PCI device.
pub struct Device {
    location: Location,
    info: DeviceInfo,
}

/// The location of a device (including function) within the bus.
#[derive(Clone, Copy, Debug)]
pub struct Location {
    pub bus: usize,
    pub device: usize,
    pub function: usize,
}

#[derive(Clone, Copy, Debug)]
pub struct DeviceInfo {
    pub vendor_id: u16,
    pub device_id: u16,
    pub class: u8,
    pub subclass: u8,
    pub prog_if: u8,
    pub header_type: u8,
}

#[derive(Clone, Copy, Debug)]
#[repr(C)]
struct DeviceConfigurationHeader {
    vendor_id: u16,
    device_id: u16,
    command: u16,
    status: u16,
    revision: u8,
    prog_if: u8,
    subclass: u8,
    class: u8,
    cache_line_size: u8,
    latency_timer: u8,
    header_type: u8,
    bist: u8,
}

impl Device {
    /// Construct a new `Device` object given it's location and
    /// info. Caller must ensure at most one `Device` object is
    /// created for a given location. Additionally, this performs port
    /// I/O. Hence, the function is unsafe.
    pub unsafe fn new(location: Location) -> Device {
        Device { location: location, info: parse_info(location) }
    }

    pub fn get_info(&self) -> DeviceInfo {
        self.info
    }
}

/// Read the PCI configuration space common header for the
/// device. Unsafe because it performs port I/O.
unsafe fn parse_info(location: Location) -> DeviceInfo {
    let mut data = [0 as u32; 4];

    for reg_ndx in 0..4 {
        data[reg_ndx] = read_config_dword(location, reg_ndx);
    }

    let header: DeviceConfigurationHeader = transmute(data);

    DeviceInfo {
        vendor_id: header.vendor_id,
        device_id: header.device_id,
        class: header.class,
        subclass: header.subclass,
        prog_if: header.prog_if,
        header_type: header.header_type,
    }
}

pub unsafe fn read_config_dword(location: Location, register: usize) -> u32 {
    let address = to_address(location.bus, location.device, location.function, register);

    outl(PCI_ADDR_PORT, address);
    inl(PCI_DATA_PORT)
}

fn to_address(bus: usize, device: usize, function: usize, register: usize) -> u32 {
    assert!(bus < 256);
    assert!(device < 32);
    assert!(function < 8);
    assert!(register < 64);

    (bus as u32) << 16
        | (device as u32) << 11
        | (function as u32) << 8
        | (register as u32) << 2
        | 1 << 31
}

const PCI_ADDR_PORT: u16 = 0xCF8;
const PCI_DATA_PORT: u16 = 0xCFC;
