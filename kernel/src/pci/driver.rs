use super::Device;

/// Matches a device that a driver supports. If a field is `None`, it
/// will match anything.
#[derive(Clone, Copy, Debug)]
pub struct DeviceMatcher {
    pub vendor_id: Option<u16>,
    pub device_id: Option<u16>,
    pub class: Option<u16>,
    pub subclass: Option<u16>,
    pub prog_if: Option<u16>,
}

/// Describes a PCI device driver. Drivers should be described with
/// this struct and registered with the PCI subsystem, which will call
/// into the driver.
pub struct DriverInfo {
    /// Used to match a device with this driver.
    pub matcher: DeviceMatcher,
    /// Called to initialize the driver for a device. Returns whether
    /// initialization was successful. If the driver can't handle this
    /// device, it should return `false`.
    pub init: unsafe fn(&mut Device) -> bool,
}
