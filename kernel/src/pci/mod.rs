mod device;
mod driver;
mod scan;

pub use self::device::Device;
pub use self::device::DeviceInfo;
pub use self::device::Location;
pub use self::driver::DeviceMatcher;
pub use self::driver::DriverInfo;

use alloc::collections::linked_list::LinkedList;
use spin::Mutex;

pub fn init() {
    scan::scan_all();
}

pub fn register_driver(driver: DriverInfo) {
    DRIVER_LIST.lock().push_back(driver);
}

pub type DeviceLock = spin::Mutex<Device>;

lazy_static! {
    static ref DEVICE_LIST: spin::Mutex<LinkedList<DeviceLock>> = {
        spin::Mutex::new(LinkedList::new())
    };
    static ref DRIVER_LIST: spin::Mutex<LinkedList<DriverInfo>> = {
        spin::Mutex::new(LinkedList::new())
    };
}
