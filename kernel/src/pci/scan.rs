use super::Device;
use super::DEVICE_LIST;
use super::Location;

/// Scan all PCI devices.
pub fn scan_all() {
    for bus in 0..256 {
        scan_bus(bus);
    }
}

/// Scan all devices on a given PCI bus.
fn scan_bus(bus_ndx: usize) {
    for dev_ndx in 0..32 {
        scan_device(bus_ndx, dev_ndx);
    }
}

/// Scan all functions on a PCI device.
fn scan_device(bus_ndx: usize, dev_ndx: usize) {
    for function_ndx in 0..8 {
        let location = Location {
            bus: bus_ndx,
            device: dev_ndx,
            function: function_ndx,
        };

        let dev = unsafe { Device::new(location) };
        let dev_info = unsafe { dev.get_info() };

        // If this is an invalid device, we skip it.
        if dev_info.class == 0xFF && dev_info.subclass == 0xFF {
            continue;
        }

        info!(
            "Device {:x} {:x} {:x}: class {:x} subclass {:x}",
            bus_ndx, dev_ndx, function_ndx, dev_info.class, dev_info.subclass
        );

        DEVICE_LIST.lock().push_back(spin::Mutex::new(dev));

        // TODO: recurse if this is a PCI-PCI bridge instead of
        // brute-forcing.

        // The 31st bit of `dev_info.header_type` indicates whether
        // this is a multi-function device. If it isn't set, don't
        // check the other functions.
        if dev_info.header_type & 0x80 == 0 {
            break;
        }
    }
}
